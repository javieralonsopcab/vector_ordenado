/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vectores;

/**
 *
 * @author Javier
 */
public class Vector {

    private int vector[];
    private int tamanio;

    public Vector(int tamanio) {
        this.vector = new int[tamanio];
    }

    public void addElementoFin(int numero) {
        if (this.vector[this.vector.length - 1] == 0) {
            this.vector[this.vector.length - 1] = numero;
            this.tamanio++;
        } else {
            this.ampliarEspacioFinal();
            this.addElementoFin(numero);
        }
    }

    public void addElementoInicio(int numero) {
        if (this.vector[0] == 0) {
            this.vector[0] = numero;
            this.tamanio++;
        } else {
            this.ampliarEspacioInicio();
            this.addElementoInicio(numero);
        }
    }

    public void sort() {
        for (int i = 0; i < this.tamanio - 1; i++) {
            for (int j = 0; j < this.tamanio - 1; j++) {
                if (this.vector[j] > this.vector[j + 1]) {
                    int tmp = this.vector[j];
                    this.vector[j] = this.vector[j + 1];
                    this.vector[j + 1] = tmp;
                }
            }
        }
    }

    public void addElementosOrdenados(int numero) {
        if (this.tamanio == 0) {
            this.addElementoInicio(numero);
        } else {
            int pocision = this.pocisionAAniadir(numero);
            this.acomodarDesdePocision(pocision);
            this.addIndex(numero, pocision);
        }
    }

    private void addIndex(int numero, int pocision) {
        this.vector[pocision] = numero;
        this.tamanio++;
    }

    private void acomodarDesdePocision(int pocision) {
        if (pocision == 0) {
            this.ampliarEspacioInicio();
        } else {
            int[] aux = new int[this.vector.length + 1];
            for (int i = 0; i < this.vector.length; i++) {
                if (i >= pocision) {
                    aux[i + 1] = this.vector[i];
                } else {
                    aux[i] = this.vector[i];
                }
            }
            this.vector = aux;
        }
    }

    private int pocisionAAniadir(int numero) {
        int index = 0;
        for (int i = 0; i < this.vector.length; i++) {
            if (this.vector[i] > numero) {
                index = i;
                break;
            } else if (i == this.tamanio) {
                index = this.tamanio;
                break;
            }
        }
        return index;
    }

    private void ampliarEspacioInicio() {
        int[] aux = new int[this.vector.length + 1];
        for (int i = 0; i < this.vector.length; i++) {
            aux[i + 1] = this.vector[i];
        }
        this.vector = aux;
    }

    private void ampliarEspacioFinal() {
        int[] aux = new int[this.vector.length + 1];
        for (int i = 0; i < this.vector.length; i++) {
            aux[i] = this.vector[i];
        }
        this.vector = aux;
    }

    public int[] getVector() {
        return vector;
    }

    public void setVector(int[] vector) {
        this.vector = vector;
    }

    public int getTamanio() {
        return tamanio;
    }

    public void setTamanio(int tamanio) {
        this.tamanio = tamanio;
    }

    
    
}
